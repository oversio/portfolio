import {
  faFacebook,
  faInstagram,
  faLinkedin,
  faTwitter,
} from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Tooltip, useTheme } from '@nextui-org/react';
import { useMemo } from 'react';
import { motion } from 'framer-motion';
import styles from './NavAction.module.scss';

const NavActionsMedia = () => {
  const { theme } = useTheme();
  const socialMedia = useMemo(() => {
    return [
      {
        icon: faLinkedin,
        name: 'LinkedIn',
        link: '',
      },
      {
        icon: faFacebook,
        name: 'Facebook',
        link: '',
      },
      {
        icon: faTwitter,
        name: 'Twitter',
        link: '',
      },
      {
        icon: faInstagram,
        name: 'Instagram',
        link: '',
      },
    ];
  }, []);

  return (
    <div className={styles.navActions}>
      {socialMedia.map(({ icon, name, link }, key) => {
        return (
          <motion.div
            whileHover={{
              scale: 1.2,
            }}
            key={key}
            style={{
              marginLeft: theme.space[11].value,
              cursor: 'pointer',
            }}
          >
            <Tooltip
              content={name}
              color='secondary'
              rounded
              placement='bottom'
            >
              <FontAwesomeIcon
                icon={icon}
                style={{
                  fontSize: theme.fontSizes.sm.value,
                }}
              />
            </Tooltip>
          </motion.div>
        );
      })}
    </div>
  );
};

export default NavActionsMedia;
