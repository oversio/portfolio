import { useMemo } from 'react';
import { Link, useTheme } from '@nextui-org/react';
import { motion } from 'framer-motion';

const NavItem = ({ title, link, isActive = false }) => {
  const { theme } = useTheme();

  const getStyle = useMemo(() => {
    if (isActive) {
      const borderBottom = `${theme.space[1].value} solid ${theme.colors.primary.value}`;
      return {
        margin: `0 ${theme.space[13].value} 0 0`,
        border: 'none',
        paddingBottom: theme.space.lg.value,
        borderBottom,
      };
    }

    return {
      margin: `0 ${theme.space[13].value} 0 0`,
      border: 'none',
      paddingBottom: theme.space.lg.value,
      borderBottom: 'none',
    };
  }, [isActive, theme.colors.primary.value, theme.space]);

  return (
    <motion.li
      style={getStyle}
      whileHover={{
        scale: 1.1,
      }}
    >
      <Link color={!isActive ? 'text' : 'primary'} href={link}>
        {title}
      </Link>
    </motion.li>
  );
};

export default NavItem;
