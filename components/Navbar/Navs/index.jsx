import { LinksList } from '../links';
import NavItem from './NavItem';
import styles from './Navs.module.scss';

const AppNavs = () => {
  const links = LinksList;

  return (
    <nav className='header-navs'>
      <ul className={styles.navbarList}>
        {links.map((link, key) => (
          <NavItem {...link} key={key} />
        ))}
      </ul>
    </nav>
  );
};

export default AppNavs;
