import AppNavs from './Navs';
import styles from './Navbar.module.scss';
import { Container } from '@nextui-org/react';
import NavActionsMedia from './Actions/NavAction';

const Navbar = () => {
  return (
    <Container responsive>
      <header className={styles.headerNavbar}>
        <AppNavs />
        <NavActionsMedia />
      </header>
    </Container>
  );
};

export default Navbar;
