export const LinksList = [
	{
		title: 'Home',
		link: '/',
		isActive: true,
	},
	{
		title: 'About',
		link: '/about',
		isActive: false,
	},
	{
		title: 'Services',
		link: '/services',
		isActive: false,
	},
	{
		title: 'Portfolio',
		link: '/portfolio',
		isActive: false,
	},
	{
		title: 'contact',
		link: '/contact',
		isActive: false,
	}
]
