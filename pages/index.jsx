import { useTheme as useNextTheme } from 'next-themes';
import { Switch, useTheme } from '@nextui-org/react';
import { AppNavbar } from '../components';

export default function Home() {
  const { setTheme } = useNextTheme();
  const { isDark, type } = useTheme();

  return (
    <>
      <AppNavbar />
      <div
        style={{
          display: 'flex',
          height: '95vh',
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        The current theme is: {type}
        <Switch
          checked={isDark}
          onChange={(e) => setTheme(e.target.checked ? 'dark' : 'light')}
        />
      </div>
    </>
  );
}
